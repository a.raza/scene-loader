﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingTrigger : MonoBehaviour
{
    #region Public Fields
    public GameObject[] m_triggers;
    #endregion

    #region Private Functions
    private void OnCollisionEnter(Collision collision)
    {
        UIManager.Instance.m_activateLoading = true;

        DeactivateTriggers();
    }

    private void DeactivateTriggers() 
    {
        foreach(GameObject trigger in m_triggers) 
        {
            trigger.SetActive(false); 
        }
    }
    #endregion
}
