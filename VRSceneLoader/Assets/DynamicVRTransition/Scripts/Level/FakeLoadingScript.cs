﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FakeLoadingScript : Singleton<FakeLoadingScript>
{
    #region Private Fields
    private float m_loadingState;
    #endregion

    #region Unity callback
    void OnEnable()
    {
        m_loadingState = 0;
    }
    #endregion

    /// <summary>
    /// Will perform the loading procedure until 
    /// </summary>
    /// <returns>The state.</returns>
    public IEnumerator LoadingState() 
    {
        if (this.gameObject.activeInHierarchy == false)
            this.gameObject.SetActive(true);

            yield return new WaitForSeconds(1.0f);
            m_loadingState += Random.Range(Time.deltaTime, 10);
            
        UIManager.Instance.LoadingPercentage(m_loadingState);

        if (m_loadingState <= 100)
        {
            StartCoroutine(LoadingState());
        }
        else
        {
            TransitionManager.Instance.DoTransition(TransitionAction.GoDark);
            StartCoroutine(TransitionManager.Instance.LoadArea(0));
        }
    }
}
