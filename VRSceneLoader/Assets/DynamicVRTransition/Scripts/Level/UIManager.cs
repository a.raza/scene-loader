﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    #region Public Fields
    [Header("Loading Canvas Movement")]
    public float m_mSpeed;
    [Header("Loading Canvas Rotation")]
    public float m_rSpeed;
    [Header("Distance B/w Cam & Loading Canvas")]
    public float m_distance;
    [Header("")]
    public GameObject m_loadingCanvas;
    public GameObject m_transitionObj;

    [SerializeField]
    public Hint[] m_hints;
    #endregion

    #region Private Fields
    private int m_currentHint;
    #endregion

    #region Internal Fields
    internal bool m_activateLoading;
    #endregion

    #region Private Fields
    private Transform m_cam;
    private RectTransform m_loadingPanel;
    private Transform m_hintArea;
    private Image m_loadingMeter;
    private const float m_defaultDistance = 4;
    private float m_maxSpeed;
    private float m_normalSpeed;
    #endregion

    #region Unity callback
    void Start()
    {
        ManageLoadingCanvas();

        m_normalSpeed = m_mSpeed;
        m_maxSpeed = m_mSpeed * 5;
    }

    void Update()
    {
        MoveCanvas();
    }
    #endregion

    #region Private Functions

    /// <summary>
    /// Will Get the loading Canvas
    /// </summary>
    private void ManageLoadingCanvas() 
    {
        if (Camera.main == null || m_loadingCanvas == null) 
        {
            this.gameObject.SetActive(false);
            Debug.Log("Main Camera Or Prefab of LoadingCanvas Not Available");
            return;
        }

        m_cam = Camera.main.transform;

        if (m_loadingPanel == null)
        {
            m_loadingPanel = Instantiate(m_loadingCanvas, null).GetComponent<RectTransform>();
            m_hintArea = m_loadingPanel.Find("HintsArea");
            m_loadingMeter = m_loadingPanel.Find("LoadingPanel").GetChild(0).GetComponent<Image>();
            m_loadingMeter.fillAmount = 0;
        }

        ManageHints();
    }

    /// <summary>
    /// Will Manage the movement and rotation of the loading Canvas
    /// </summary>
    private void MoveCanvas()
    {
        if (m_loadingPanel == null)
        {
            ManageLoadingCanvas();
            return;
        }
        //Will activate the Loading Canvas on the Scene as according to the value.
        m_loadingPanel.gameObject.SetActive(m_activateLoading);

        // is Loading Canvas active..?
        if (m_activateLoading == true)
        {
            //Manage the distance b/w the cam and the canvas
            if (m_distance <= 0)
                m_distance = m_defaultDistance;

            //Using Player Controls..?
            if(PlayerControls.Instance != null)
                //manipulate speed accordingly
                m_mSpeed = PlayerControls.Instance.Is_usingControls() == true ? m_maxSpeed : m_normalSpeed;

            var point_a = (m_cam.forward * m_distance);
            var point_b = m_cam.position;

            //z axis of the loading canvas
            var currentPoint = point_a + point_b;
            MovementController(currentPoint);

            var distance = m_loadingPanel.position - point_b;
            RotationController(distance);
        }
        else
            return;
    }

    /// <summary>
    /// Loading canvas will move while managing the distance between the cam and the loading canvas
    /// </summary>
    /// <param name="currentPoint">Current point.</param>
    private void MovementController(Vector3 currentPoint) 
    {
        m_loadingPanel.position = Vector3.MoveTowards(m_loadingPanel.position, currentPoint, Speed(m_mSpeed));
    }

    /// <summary>
    /// Rotate the loading canvas, maintaining the panel looking at the cam
    /// </summary>
    /// <param name="distance">Distance.</param>
    private void RotationController(Vector3 distance) 
    {
        var point_a = m_loadingPanel.rotation;
        var point_b = Quaternion.LookRotation(distance);

        var rotation = point_a.eulerAngles - point_b.eulerAngles;

        if (rotation.magnitude >= m_normalSpeed)
            m_loadingPanel.rotation = Quaternion.RotateTowards(point_a, point_b, Speed(m_rSpeed));
    }

    /// <summary>
    /// This method is used to maintain the speed of different elemets
    /// such as movement and rotation
    /// </summary>
    /// <returns>The speed.</returns>
    /// <param name="speed">Speed.</param>
    private float Speed(float speed)
    {
        return speed * Time.deltaTime;
    }

    /// <summary>
    /// Will manage the hints shown in the loading panel
    /// </summary>
    private void ManageHints()
    {
        //are hints given..?
        if (m_hints.Length <= 0)
        {
            //Add some hints
            m_hints = new Hint[2];

            m_hints[0] = new Hint
            { 
                id = 0, tip = "No Hints Added In The Loading Phase" 
            };

            m_hints[1] = new Hint
            {
                id = 1,
                tip = "Here You Can Add Hints/Tips For User"
            };
        }
    }

    #endregion

    #region Event Functions

    /// <summary>
    /// Will display the hints randomly in the loading panel
    /// </summary>
    internal void GetHint() 
    {
        int rand;

        while (true)
        {
            rand = Random.Range(0, m_hints.Length);
            if (rand != m_currentHint)
            {
                m_currentHint = rand;
                Debug.Log(m_currentHint);
                break;
            }
        }

        m_hintArea.GetComponent<Text>().text = m_hints[m_currentHint].tip;

    }

    internal void LoadingPercentage(float percentage) 
    {
        m_loadingMeter.fillAmount = percentage/100;
    }
    #endregion

}

[System.Serializable]
public class Hint 
{
    public int id;
    public string tip; 
}