﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TransitionManager : Singleton<TransitionManager>
{
    #region public Fields
    public GameObject m_transitionPrefab;
    public float m_delay;
    #endregion

    #region Private Fields
    private Camera m_cam;
    private Canvas m_transitionPanel;
    private Animator m_animTransition;
    private Transform m_tPanel;
    #endregion

    #region Unity callback
    void Start()
    {
        if (m_cam == null)
            GetCam(); //Will get the main camera in the Scene
        else
            return;
    }

    #endregion

    #region Private Functions

    /// <summary>
    /// Will Get the main camera available in the Scene
    /// </summary>
    private void GetCam() 
    {
        //is main cam available..?
        if (Camera.main == null) 
        {
            this.gameObject.SetActive(false);
            Debug.Log("Main Camera Not Found");
        }

        //Get the main cam
        m_cam = Camera.main;
        //Apply transition Panel as the child to the cam
        m_transitionPanel = Instantiate(m_transitionPrefab, m_cam.transform, true).GetComponent<Canvas>();

        // Get the main transition..
        m_tPanel = m_transitionPanel.transform.GetChild(0);

        // get "Animator" Component to access animations of Transition
        m_animTransition = m_tPanel.GetComponent<Animator>();
    }

    #endregion

    #region Internal Functions

    /// <summary>
    /// Play an animation state as according to the parameter
    /// </summary>
    /// <param name="action">Action.</param>
    internal void DoTransition(TransitionAction action)
    {
        DoTransition((int)action);
    }

    /// <summary>
    /// Play an animation state as according to the parameter
    /// And Proceed to Certain Scene with a delay
    /// </summary>
    /// <param name="action">Action.</param>
    /// <param name="sceneIndex">Scene Index.</param>
    internal void DoTransition(TransitionAction action, int sceneIndex)
    {
        DoTransition((int)action);
        StartCoroutine(LoadArea(sceneIndex));
    }

    /// <summary>
    /// Proceed to another scene, and play an animation accordingly
    /// </summary>
    /// <returns>The area.</returns>
    /// <param name="sceneIndex">Scene index.</param>
    internal IEnumerator LoadArea(int sceneIndex) 
    {
        yield return new WaitForSeconds(m_delay);

        //Proceed to another scene
        SceneManager.LoadScene(sceneIndex);

        yield return new WaitForSeconds(Time.deltaTime);

        //Get the Cam in the new scene
        GetCam();

        //Proceed with a transition animation accordingly
        DoTransition(TransitionAction.GoVisible);

        if (FakeLoadingScript.Instance != null)
        {
            FakeLoadingScript.Instance.gameObject.SetActive(false);
        }

        if (UIManager.Instance != null)
        {
            UIManager.Instance.m_activateLoading = false;
        }
    }

    /// <summary>
    /// Check if certain transition animation state is in progress
    /// </summary>
    /// <returns><c>true</c>, if in transition was ised, <c>false</c> otherwise.</returns>
    /// <param name="action">Action.</param>
    internal bool IsInTransition(TransitionAction action) 
    {
        var state = m_animTransition.GetCurrentAnimatorStateInfo(0).IsName(action.ToString());

        if (state.Equals(true)) 
            return true;
        return false;
    }

    /// <summary>
    /// Play an animation state as according to the parameter
    /// </summary>
    /// <param name="action">Action.</param>
    internal void DoTransition(int action)
    {
        // is main transition panel is disabled..?
        if (m_tPanel.gameObject.activeInHierarchy == false)
            //Enable the main transition panel
            m_tPanel.gameObject.SetActive(true);

        //proceed to certain animation state
        m_animTransition.SetInteger("anim", action);
    }
    #endregion

}

public enum TransitionAction 
{ 
    GoDark = 1,
    GoVisible = 0,
};
