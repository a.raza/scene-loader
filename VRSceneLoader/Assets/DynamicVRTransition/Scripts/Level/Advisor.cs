﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Advisor : MonoBehaviour
{
    #region Private Functions
    public void GetHint() 
    {
        UIManager.Instance.GetHint();

        //For Testing Only
        TestingLoadingNTransition();
    }
    #endregion

    #region Public Functions
    private void TestingLoadingNTransition() 
    {
        TransitionManager.Instance.m_delay = 6;
        TransitionManager.Instance.m_transitionPrefab = UIManager.Instance.m_transitionObj;
        StartCoroutine(FakeLoadingScript.Instance.LoadingState());
    }
    #endregion
}
