﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Demo1 : MonoBehaviour
{
    #region UI Event Functions
    /// <summary>
    /// Proceed to certain Scene with certain amount of delay
    /// </summary>
    /// <param name="sceneName">Scene name.</param>
    public void Proceed2CertainArea(Dropdown dropdown)
    {
        var value = dropdown.value;

        StartCoroutine(TransitionManager.Instance.LoadArea(value));
    }

    /// <summary>
    /// Play an animation state as according to the parameter
    /// </summary>
    /// <param name="action">Action.</param>
    public void DoTransition(int action) 
    {
        TransitionManager.Instance.DoTransition(action); 
    }

    /// <summary>
    /// Enables the user interface.
    /// </summary>
    /// <param name="userInterface">User interface.</param>
    public void EnableUI(GameObject userInterface) 
    {
        StartCoroutine(EnableUserInterface(userInterface));
    }

    /// <summary>
    /// Enables the user interface after a slight delay
    /// </summary>
    /// <returns>The user interface.</returns>
    /// <param name="userInterface">User interface.</param>
    private IEnumerator EnableUserInterface(GameObject userInterface) 
    {
        yield return new WaitForSeconds(10f);

        userInterface.SetActive(true);
    }

    #endregion
}