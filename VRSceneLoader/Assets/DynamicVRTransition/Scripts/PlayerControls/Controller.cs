﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour
{
    #region Private Fields
    private float xaxis, zaxis;
    private Vector3 m_postion;
    #endregion

    #region Public Fields
    private Transform m_cam;
    private Transform m_taller;
    #endregion

    #region Unity callback
    internal void Start()
    {
        GetCam();
        m_postion = Vector3.zero;
    }

    void Update()
    {
        if (m_cam == null)
        {
            Start();
            return;
        }

        if(m_taller == null) 
        {
            this.gameObject.SetActive(false);
            Debug.Log("Cam Parent Not Found");
            return;
        }

        m_taller.position = transform.position;
        m_postion = Position();
    }
    #endregion

    #region Private Functions
    private void GetCam()
    {
        if (Camera.main == null)
        {
            this.gameObject.SetActive(false);
            Debug.LogError("Main Camera Not Found");
        }

        //Get the main cam
        m_cam = Camera.main.transform;
        m_taller = m_cam.parent;
    }

    private Vector3 Position()
    {
        xaxis = Input.GetAxis("Horizontal");
        zaxis = Input.GetAxis("Vertical");
        bool x = xaxis != 0;
        bool y = zaxis != 0;

        // if player is using the controls...
        if (x||y||x&&y)
        {
            Vector3 forward = m_cam.TransformDirection(Vector3.forward);

            // Player is moving on ground, Y component of camera facing is not relevant.
            forward.y = 0.0f;
            forward = forward.normalized;

            // Calculate target direction based on camera forward and direction key.
            Vector3 right = new Vector3(forward.z, 0, -forward.x);
            Vector3 targetDirection;
            targetDirection = forward * zaxis + right * xaxis;

            return targetDirection;
        }
        else
        {
            return Vector3.zero;
        }
    }
    #endregion

    #region Public Functions
    public Vector3 GetPosition()
    {
        return m_postion;
    }
    #endregion

}
