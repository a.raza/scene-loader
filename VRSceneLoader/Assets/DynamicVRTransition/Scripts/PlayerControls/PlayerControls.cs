﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : Singleton<PlayerControls>
{
    #region Public Fields
    public float m_speed;
    #endregion

    #region Private Fields
    private Controller m_controller;
    private Vector3 m_targetDir, m_dir;
    #endregion

    #region Unity callback
    void Start()
    {
        m_controller = GetComponent<Controller>();
    }

    void Update()
    {
        Controls();     // allow to use controller  
    }

    #endregion

    #region Private Functions
    /// <summary>
    /// Will control User Movement
    /// </summary>
    private void Controls()
    {
        // if controller is in use.. // and not in the state of get hit
        if (Is_usingControls())
        {
            //allow user to move
            Move();
        }
    }

    private void Move()
    {
        // This will move the character according to the controller
        transform.position += new Vector3
            (
                (m_controller.GetPosition().x * m_speed * Time.deltaTime), //updating horizontal movement
                0,
                (m_controller.GetPosition().z * m_speed * Time.deltaTime) //updating Vertical movement
            );
    }
    #endregion

    #region Internal Functions
    //following will be used to check either player is using movement controls or not
    internal bool Is_usingControls()
    {
        return m_controller.GetPosition() != Vector3.zero;
    }
    #endregion
}
