﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    #region Public Fields
    public float m_speedH = 2.0f;
    public float m_speedV = 2.0f;
    #endregion

    #region Private Fields
    private float m_yaw = 0.0f;
    private float m_pitch = 0.0f;
    #endregion

    #region Unity callback

    void LateUpdate()
    {
        MoveCam();
    }
    #endregion

    #region Private Functions

    /// <summary>
    /// Rotate the Cam as According mouse position
    /// </summary>
    private void MoveCam() 
    {
        m_yaw += m_speedH * Input.GetAxis("Mouse X");
        m_pitch -= m_speedV * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(m_pitch, m_yaw, 0.0f);
    }
    #endregion

    //#region internal Functions
    //internal bool IsUsingCam()
    //{
    //    bool x = Input.GetAxis("Mouse X") != 0;
    //    bool y = Input.GetAxis("Mouse Y") != 0;

    //    return x || y || x && y ? true : false;
    //}
    //#endregion

}